# WIN10+CUDA10.2+CUDNN+Pytorch安装教程

[TOC]

## 信息查询和文件准备

		### 查询CUDA版本

打开英伟达控制面板，点击左下角系统信息，选择组件，查询CUDA版本<img src="D:\Typera\Python\CUDA安装\1.png" style="zoom: 25%;" />

此处为所支持的CUDA的最高版本

### 1.下载所需文件

因为浏览器下载速度偏慢,强烈建议用迅雷下载,基本可以满速下载

#### 2.CUDA10.2

前往https://developer.nvidia.com/cuda-10.2-download-archive,下载CUDA10.2

1. <img src="D:\Typera\Python\CUDA安装\2.png" style="zoom: 25%;" />


只需在箭头处右键,复制链接,然后将链接粘贴到迅雷下载即可

此处粘贴下载的链接,不过还是建议去官网的比较好:

Base Installer:https://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda_10.2.89_441.22_win10.exe

Patch 1:https://developer.download.nvidia.com/compute/cuda/10.2/Prod/patches/1/cuda_10.2.1_win10.exe

Patch 2:https://developer.download.nvidia.com/compute/cuda/10.2/Prod/patches/2/cuda_10.2.2_win10.exe

#### 2.CUDNN

前往https://developer.nvidia.com/cudnn,下载CUDNN

加载较慢,请耐心等待,另外,下载CUDNN需要登入英伟达账号

<img src="D:\Typera\Python\CUDA安装\5.png" style="zoom:25%;" />

在此处右键,复制链接到迅雷下载即可

#### 3.Pytorch文件

使用pip下载速度慢,此处建议下载到本地,然后安装

打开pytorch官网,选择如下图:<img src="D:\Typera\Python\CUDA安装\3.png" style="zoom: 25%;" />

复制此段到cmd,回车,开始下载,肉眼可见的下载速度慢.

接下来复制其中显示的一段链接(如图所示)到迅雷中下载,然后从本地安装就行了![](D:\Typera\Python\CUDA安装\4.png)

要安装的文件一共有三个,后两个文件的大小只有几兆,可以不用迅雷.



